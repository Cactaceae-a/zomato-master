//libraries
import express from "express";


//database model
import { RestaurantModel } from "../../database/allModel";

//validation
import {ValidateId} from "../../Validation/common"
import {ValidateRestaurantCity, ValidateRestaurantSearchString} from "../../Validation/restaurant"

const Router= express.Router();
/**
 * Router       /
 * Des          Get all restraunats detail based on the city
 * Params       none
 * Access       Public
 * Method       GET
 */

    Router.get('/', async (req,res)=>{
        try{
        // await ValidateRestaurantCity(req.query);
        //http://localhost:5000/restaurant/?city=ncr
        const {city} = req.query;
        const restraunats = await RestaurantModel.find({city});
        if(restraunats.length==0){
            res.json({error: "No restaurant found in this city"});
        }
        return res.json({restraunats});
        }catch(error){
        res.status(500).json({error: error.message});
        }
    });

 /**
 * Router       /:_id
 * Des          Get single restaurant details based on id
 * Params       none
 * Access       Public
 * Method       GET
 */
//http://localhost:5000/restaurant/1235431249ndjaepofja
Router.get('/:_id', async (req,res)=>{
    try{
        await ValidateId(req.params);
        const {_id} = req.params;
        const restraunat = await RestaurantModel.findById({_id});
        if(!restraunat){
           return res.status(400).json({error: "Restaurant not found"});
        }
        return res.json({restraunat});
        }catch(error){
        res.status(500).json({error: error.message});
        }
});

 /**
 * Router       /search
 * Des          Get restaurant details based on search string
 * Params       none
 * Access       Public
 * Method       GET
 */
Router.get('/search/:searchString', async (req,res)=>{
    try{
        await ValidateRestaurantSearchString(req.params);
        const searchString= req.params;
        const restaurants= await RestaurantModel.find({
            name: {$regex: searchString, $options: "i"},
        });
        if(!restaurants) return res.status(404).json({error: `No restraunt matched with ${searchString}`});
        return res.json({restaurants});
        }catch(error){
        res.status(500).json({error: error.message});
        }
})

export default Router;