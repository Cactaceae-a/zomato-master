// Library
import  express  from "express";
import passport from "passport";

//models
import { userModel } from "../../database/allModel";

//validation
import {ValidateSignup, ValidateSignin} from "../../Validation/auth"

//create a router
const Router = express.Router();
/**
 *  Router       /signup
 * des           register new user
 * params        none
 * access        Public
 * METHOD        Post
*/
Router.post("/signup", async (req, res)=>{
    try{
    // await ValidateSignup(req.body.credentials);
    await userModel.findByEmailAndPhone(req.body.credentials);
    const newUser = await userModel.create(req.body.credentials);
    const token = newUser.generateJwtToken();
    return res.status(200).json({ token, status: "success" });
    } catch(error){
     return res.status(500).json({error: error.message});
    }
});

/**
 *  Router       /signin
 * des           sign in with email and password
 * params        none
 * access        Public
 * METHOD        Post
*/

Router.post("/signin", async (req, res)=>{
try{
  await ValidateSignin(req.body.credentials);
const user = await userModel.findByEmailAndPassword(req.body.credentials);
const token = user.generateJwtToken();
return res.status(200).json({ token, status: "success" });
}catch(error){
     return res.status(500).json({error: error.message});
    }
});

/**
 *  Router       /google
 * des           google signin
 * params        none
 * access        Public
 * METHOD        get
*/

Router.get("/google", passport.authenticate("google", {
   scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email"
    ]
}));

/**
 * Router       /google/callback
 * Des          Google signin callback
 * Params       none
 * Access       Public
 * Method       GET
 */
 Router.get("/google/callback", passport.authenticate("google", { failureRedirect: "/" }),
    (req, res) => {
        // return res.status(200).json({token: req.session.passport.user.token, status: "success"})
      return res.redirect(
        `http://localhost:3000/google/${req.session.passport.user.token}`
      );
    }
  );



export default Router;

