//library
import express from 'express';
import AWS from 'aws-sdk';
import multer from 'multer';


//Database model
import { ImageModel } from '../../database/allModel';

const Router = express.Router();

//multer config
const storage = multer.memoryStorage();
const upload = multer({storage});



//utility function
import {s3Upload} from '../../utils/s3'

/**
 * Router       /image
 * Des          Uploads given image to S3 bucket and save the file name to mongodb
 * Params       none
 * Access       Public
 * Method       POST
 */
Router.post('/', upload.single("file"), async(req, res)=>{
    try{
        const file= req.file;
       
        //s3 bucket options
        const basketOption ={
            Bucket: "zomato-master-new",
            Key: file.originalname,
            Body: file.buffer,
            contentType: file.mimetype,
            ACL: "public-read" //Access control List  
        };

      const uploadImage= await s3Upload(basketOption);
      
      const saveImageToDatabase = await ImageModel.create({
        images: [{ location: uploadImage.Location }],
      });

        return res.status(200).json(saveImageToDatabase);
       
    }catch(error){
        res.status(500).json({error: error.message});
    }
})

Router.get('/:_id', async(req, res)=>{
  try{
  const {_id} = req.params;
  const image = await ImageModel.findById(_id);
   return res.status(200).json(image);
     
  }catch(error){
      res.status(500).json({error: error.message});
  }
})


export default Router;