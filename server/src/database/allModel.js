import { FoodModel } from "./food";
import { ImageModel } from "./image";
import { MenuModel } from "./menu";
import { OrderModel } from "./order";
import { RestaurantModel } from "./restaurant";
import { ReviewModel } from "./reviews";
import { userModel } from "./user";



export{
    FoodModel,
    ImageModel,
    MenuModel,
    OrderModel,
    RestaurantModel,
    ReviewModel,
    userModel
}

  