"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _allModel = require("../../database/allModel");

var _common = require("../../Validation/common");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Router = _express["default"].Router();
/**
 * Router       /list/:_id
 * Des          Get all the list of menu based on restaurant id
 * Params       none
 * Access       Public
 * Method       GET
 */


Router.get("/list/:_id", /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var _id, _menu;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return (0, _common.ValidateId)(req.params);

          case 3:
            _id = req.params._id;
            _context.next = 6;
            return _allModel.MenuModel.findById({
              _id: _id
            });

          case 6:
            _menu = _context.sent;

            if (_menu) {
              _context.next = 9;
              break;
            }

            return _context.abrupt("return", res.status(404).json({
              error: "no menu present for this resturant"
            }));

          case 9:
            return _context.abrupt("return", res.json({
              menu: _menu
            }));

          case 12:
            _context.prev = 12;
            _context.t0 = _context["catch"](0);
            res.status(500).json({
              error: _context.t0.message
            });

          case 15:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 12]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
/**
 * Router       /image/:id
 * Des          Get all the list of menu images with restaurant id
 * Params       none
 * Access       Public
 * Method       GET
 */

Router.get("/image/:_id", /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _id, menuImages;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return (0, _common.ValidateId)(req.params);

          case 3:
            _id = req.params._id;
            _context2.next = 6;
            return _allModel.MenuModel.findById({
              _id: _id
            });

          case 6:
            menuImages = _context2.sent;

            if (menuImages) {
              _context2.next = 9;
              break;
            }

            return _context2.abrupt("return", res.status(404).json({
              error: "no menu present for this resturant"
            }));

          case 9:
            return _context2.abrupt("return", res.json({
              menu: menu
            }));

          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            res.status(500).json({
              error: _context2.t0.message
            });

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 12]]);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
var _default = Router;
exports["default"] = _default;