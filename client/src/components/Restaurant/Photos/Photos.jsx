import React, { useState, useEffect } from "react";
import ImageViewer from "react-simple-image-viewer";

// redux
import { useSelector, useDispatch } from "react-redux";
import { getImage } from "../../../redux/reducers/image/image.action";

// component
import PhotoCollection from "./PhotoCollection";

function Photos() {
  const [photos, setPhotos] = useState([]);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [currentImage, setCurrentImage] = useState(0);

  const closeViewer = () => setIsMenuOpen(false);
  const openViewer = () => setIsMenuOpen(true);

  const reduxState = useSelector(
    (globalState) => globalState.restaurant.selectedRestaurant.restraunat
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (reduxState)
      dispatch(getImage(reduxState?.menuImages)).then((data) => {
        const images = [];
        data.payLoad.images.map(({ location }) => images.push(location));
        setPhotos(images);
      });
  }, [reduxState]);

  return (
    <>
      {isMenuOpen && (
        <ImageViewer
          src={photos}
          currentIndex={currentImage}
          disableScroll={false}
          onClose={closeViewer}
        />
      )}

      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-3">
        {photos.map((photo, index) => (
          <PhotoCollection
            key={index}
            openViewer={openViewer}
            index={index}
            image={photo}
            setCurrentImage={setCurrentImage}
          />
        ))}
      </div>
    </>
  );
}

export default Photos;

//redux state
// const [photos, setPhotos] = useState([
//   "https://b.zmtcdn.com/data/pictures/chains/9/2400009/d0e0e120b2887e69e9d6dcbe1f1f1d8d.png",
//   "https://b.zmtcdn.com/data/pictures/chains/9/2400009/1770f8f7e7924305c2812c512596ca8e.jpg",
//   "https://b.zmtcdn.com/data/pictures/chains/9/2400009/77f5380da7d5ad1649c6f6be3bdf5c71.jpg",
//   "https://b.zmtcdn.com/data/reviews_photos/fd8/cd8fc5403f2671d88f3f780e252f0fd8_1571651831.jpg",
//   "https://b.zmtcdn.com/data/reviews_photos/fe4/a67e80c6e77103cff8e89a5c7f586fe4_1443370302.jpg",
//   "https://b.zmtcdn.com/data/reviews_photos/fe4/a67e80c6e77103cff8e89a5c7f586fe4_1443370302.jpg",
//   "https://b.zmtcdn.com/data/reviews_photos/fd8/cd8fc5403f2671d88f3f780e252f0fd8_1571651831.jpg",

// ]);