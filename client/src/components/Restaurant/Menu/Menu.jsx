import React, { useState, useEffect } from "react";

// redux
import { useSelector, useDispatch } from "react-redux";
import { getImage } from "../../../redux/reducers/image/image.action";

// components
import MenuCollection from "../MenuCollection";

function Menu() {
  const dispatch = useDispatch();
  const [menus, setMenu] = useState([]);
  const reduxState = useSelector(
    (globalState) => globalState.restaurant.selectedRestaurant.restraunat
  );

  useEffect(() => {
    if (reduxState)
      dispatch(getImage(reduxState?.menuImages)).then((data) => {
        const images = [];
        data.payLoad.images.map(({ location }) => images.push(location));
        setMenu(images);
      });
  }, [reduxState]);

  return (
    <div className="flex flex-wrap gap-3">
      <MenuCollection menuTitle="Menu" pages={menus.length} image={menus} />
    </div>
  );
}

export default Menu;

//redux data reference
// const [menus, setMenu] = useState([
//   "https://b.zmtcdn.com/data/menus/053/2400053/3a62d1d52a4133881eb0ba043da3f244.jpg",
//   "https://b.zmtcdn.com/data/menus/053/2400053/0e81327dc71d3c89636b1ff17ad1e7a1.jpg",
//   "https://b.zmtcdn.com/data/menus/053/2400053/2d8f9387456ee27d86ad55534f6759e6.jpg"
// ]);