import React, { useState, useEffect } from "react";
import { AiOutlineCompass } from "react-icons/ai";
import { BiTimeFive } from "react-icons/bi";

// redux
import { useSelector, useDispatch } from "react-redux";
import { getFoodList } from "../../redux/reducers/food/food.action";

// components
import FloatMenuBtn from "./Order-Online/FloatMenuBtn";
import FoodList from "./Order-Online/FoodList";
import MenuListContainer from "./Order-Online/MenuListContainer";

function OrderOnline() {
  const [menu, setMenu] = useState([]);
  const [selected, setSelected] = useState("Recommended");

  const onClickHandler = (e) => {
    if (e.target.id) {
      setSelected(e.target.id);
    }
    return;
  };

  const dispatch = useDispatch();

  const reduxState = useSelector(
    (globalState) => globalState.restaurant.selectedRestaurant.restraunat
  );

  useEffect(() => {
    reduxState &&
      dispatch(getFoodList(reduxState.menu)).then((data) => {
        setMenu(data.payload.menu.menus);
      });
  }, [reduxState]);
  console.log(menu)

  return (
    <>
      <div className="w-full h-screen flex">
        <aside className="hidden md:flex flex-col gap-1 border-r overflow-y-scroll border-gray-200 h-screen w-1/4">
          {menu.map((item, index) => (
            <MenuListContainer
              {...item}
              key={index}
              onClickHandler={onClickHandler}
              selected={selected}
            />
          ))}
        </aside>
        <div className="w-full px-3 md:w-3/4">
          <div className="pl-3 mb-4">
            <h2 className="text-xl font-semibold">Order Online</h2>
            <h4 className="flex items-center gap-2 font-light text-gray-500">
              <AiOutlineCompass /> Live Track Your Order | <BiTimeFive /> 45 min
            </h4>
          </div>
          <section className="flex h-screen overflow-y-scroll flex-col gap-3 md:gap-5">
            {menu.map((item, index) => (
              <FoodList key={index} {...item} />
            ))}
          </section>
        </div>
      </div>
      <FloatMenuBtn
        menu={menu}
        onClickHandler={onClickHandler}
        selected={selected}
      />
    </>
  );
}

export default OrderOnline;


//redux data reference
// name: "Recommended",
// items: [
//   {
//     name: "Chole Bhature",
//     image: "https://b.zmtcdn.com/data/reviews_photos/cc7/9eacf20d6a8419412f2c30343b7ffcc7_1632490369.jpg",
//     isAddedToCart: false,
//     rating: 4,
//     price: "100",
//     description: "2 Bhature witha a piece of delicious Chole served with Pickle"
//   },
//   {
//     name: "Jalebi",
//     image: "https://b.zmtcdn.com/data/dish_photos/971/cd9ea8bc818eb658095c50806c8d2971.png",
//     isAddedToCart: true,
//     rating: 4,
//     price: "100",
//     description: "5 pieces of jalebi served with dahi"
//   },
//   {
//     name: "Khasta Dum Aloo",
//     image: "https://b.zmtcdn.com/data/dish_photos/248/ecd074c49e1696899c0e868e5460b248.jpg",
//     isAddedToCart: false,
//     rating: 4,
//     price: "35",
//     description: "Khasta [1 Pc] Dum Aloo"
//   },
//   {
//     name: "Samosa",
//     image: "https://b.zmtcdn.com/data/dish_photos/875/341b1e09ef2c61d16cb3507523ac4875.jpg",
//     isAddedToCart: false,
//     rating: 4,
//     price: "10",
//     description: "1 piece of delicious samosa with green and red chuttney"
//   },
// ]
// },
// {
// name: "Momos",
// items: [
//   {
//     name: "Jalebi",
//     image: "https://b.zmtcdn.com/data/dish_photos/971/cd9ea8bc818eb658095c50806c8d2971.png",
//     isAddedToCart: true,
//     rating: 4,
//     price: "100",
//     description: "5 pieces of jalebi served with dahi"
//   },

// ]
// },
// {
// name: "Burgers and Sandwiches ",
// items: [
//   {
//     name: "Khasta Dum Aloo",
//     image: "https://b.zmtcdn.com/data/dish_photos/248/ecd074c49e1696899c0e868e5460b248.jpg",
//     isAddedToCart: false,
//     rating: 4,
//     price: "35",
//     description: "Khasta [1 Pc] Dum Aloo"
//   },
// ]
// },
// {
// name: "Snacks",
// items: [
//   {
//     name: "Khasta Dum Aloo",
//     image: "https://b.zmtcdn.com/data/dish_photos/248/ecd074c49e1696899c0e868e5460b248.jpg",
//     isAddedToCart: false,
//     rating: 4,
//     price: "35",
//     description: "Khasta [1 Pc] Dum Aloo"
//   },
// ]
// },
// {
// name: "Desserts",
// items: [{
//   name: "Khasta Dum Aloo",
//   image: "https://b.zmtcdn.com/data/dish_photos/248/ecd074c49e1696899c0e868e5460b248.jpg",
//   isAddedToCart: false,
//   rating: 4,
//   price: "35",
//   description: "Khasta [1 Pc] Dum Aloo"
// },
// ]