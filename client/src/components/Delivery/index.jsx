import React, {useEffect, useState} from 'react'
import { useSelector } from 'react-redux'

import Slider from 'react-slick'

//Component
import DeliveryCarousel from "./DeliveryCarousel"
import RestaurantCard from '../RestaurantCard'


function Delivery() {
const [restaurantList, setRestaurantList] = useState([]);

const reduxState= useSelector((store)=>{

  return(store.restaurant.restaurants)
})


useEffect(()=>{
reduxState.restraunats && setRestaurantList(reduxState.restraunats)
}, [reduxState.restraunats])
 

  return (
    <>
        <DeliveryCarousel/>
        <h1 className='text-xl mt-4 mb-2 md:mt-8 md:text-3xl md:font-semibold'>
        Delivery Restaurants in NCR(Delhi)  
        </h1>
        <div className='flex justify-between flex-wrap mt-5'>
           {restaurantList.map((restaurant)=>(
             <RestaurantCard {...restaurant} key={restaurant._id} />
           ))}
        </div>
    </>
  )
}

export default Delivery

//redux reference data
// {
//   _id: "1234556",
//   image: {
//     images: [
//       {
//         location: "https://www.bakingbusiness.com/ext/resources/2019/8/08192019/GlobalTrends.jpg?t=1566494557&width=1080"
//       }
//     ],
//   },
//   name: "BakerHouse Comfort",
//   cuisine: ["Bakery", "Desserts", "Fast Food"],
//   isPro: false,
//   isOff: true,
//   DurationOfDelivery: 47,
//   RestaurantReviewValue: 4.1,
// },