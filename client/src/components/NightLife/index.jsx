import React from 'react'

//component
import NightLifeCaraousel from './NightLifeCaraousel'

function Dining() {
  return (
    <div>
        <h1 className="text-xl my-4 md:my-8 md:text-3xl md:font-semibold">
        Nightlife Restaurant in DELHI NCR
      </h1>
      <NightLifeCaraousel/>
    </div>
  )
}

export default Dining