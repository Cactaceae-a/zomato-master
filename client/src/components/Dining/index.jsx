import React from 'react'

//component
import DiningCaraousel from './DiningCaraousel'

function Dining() {
  return (
    <div>
        <h1 className="text-xl my-4 md:my-8 md:text-3xl md:font-semibold">
        Dine-Out Restaurants in DELHI NCR
      </h1>
      <DiningCaraousel/>
    </div>
  )
}

export default Dining