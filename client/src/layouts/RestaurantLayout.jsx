import React, { useState, useEffect } from 'react'
import { TiStarOutline } from "react-icons/ti";
import { RiDirectionLine, RiShareForwardLine } from "react-icons/ri";
import { BiBookmarkPlus } from "react-icons/bi";
import { useParams } from "react-router-dom";

//redux
import { useDispatch } from 'react-redux';
import { getSpecificRestaurant } from '../redux/reducers/restaurant/restaurant.action';
import { getImage } from '../redux/reducers/image/image.action';
import { getCart } from '../redux/reducers/cart/cart.action';

//components
import Navbar from '../components/Navbar'
import ImageGrid from '../components/Restaurant/ImageGrid'
import RestaurantInfo from '../components/Restaurant/RestaurantInfo'
import InfoButton from '../components/Restaurant/InfoButton'
import Tabs from '../components/Restaurant/Tabs'
import CartContainer from '../components/Cart/CartContainer';

function RestaurantLayout({children}) {
    const [restaurant, setRestaurant] = useState({
        images: [],
        name: "",
        cuisine: "",
        address: "",
        restaurantRating: 4.1,
        deliveryRating: 3.2,
      });
      const { id } = useParams();
     
      const dispatch = useDispatch();
      
       useEffect(()=>{
        dispatch(getSpecificRestaurant(id)).then((data)=>{
          setRestaurant((prev)=>({
            ...prev, ...data.payload.restraunat
            }));
          
            dispatch(getImage(data.payload.restraunat.photos)).then((data)=>{
              setRestaurant((prev)=>({
                ...prev, images: data.payLoad.images
              }));
            });
        });
        dispatch(getCart());
       },[])


  return (
    <>
        <Navbar/>
        <div className='container mx-auto px-4 mt-8 lg:px-20 pb-20'>
        <ImageGrid images={restaurant.images} />
        <RestaurantInfo
          name={restaurant?.name}
          restaurantRating={restaurant?.restaurantRating || 0}
          deliveryRating={restaurant?.deliveryRating || 0}
          cuisine={restaurant?.cuisine}
          address={restaurant?.address}
        />
        <div className="my-4 flex flex-wrap gap-3 mx-auto">
          <InfoButton isActive={true}>
            <TiStarOutline /> Add Review
          </InfoButton>
          <InfoButton>
            <RiDirectionLine /> Direction
          </InfoButton>
          <InfoButton>
            <BiBookmarkPlus /> Bookmark
          </InfoButton>
          <InfoButton>
            <RiShareForwardLine /> Share
          </InfoButton>
        </div>
        <div className="my-10">
          <Tabs />
        </div>
        {children}
        </div>

        <CartContainer />
        
       
    </>
  )
}

export default RestaurantLayout

//redux reference data
// images: [
//   "https://b.zmtcdn.com/data/pictures/9/2400009/33d6b7973c6645f001a1e35390f5ea26.jpg",
//   "https://b.zmtcdn.com/data/pictures/chains/9/2400009/d0e0e120b2887e69e9d6dcbe1f1f1d8d.png",
//   "https://b.zmtcdn.com/data/reviews_photos/fe4/a67e80c6e77103cff8e89a5c7f586fe4_1443370302.jpg",
//   "https://b.zmtcdn.com/data/pictures/chains/9/2400009/d0e0e120b2887e69e9d6dcbe1f1f1d8d.png",
//   "https://b.zmtcdn.com/data/reviews_photos/fe4/a67e80c6e77103cff8e89a5c7f586fe4_1443370302.jpg"
// ],
// name: "BakeHouse Comfort",
// cuisine: ["Backery", "Desserts", "Fast Food"],
// address: "Biriyani, Hyderabadi, Andhra, North Indian, Chinese, Desserts",
// restaurantRating: 4.1,
// deliveryRating: 3.2,